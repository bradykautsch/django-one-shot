from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm


# Create your views here.
def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_object": todo_lists,
    }
    return render(request, "todos/list.html", context)


def show_todo_list(request, id):
    todo_item = get_object_or_404(TodoList, id=id)
    context = {
        "todo_item_object": todo_item,
        }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    edit = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
        form = TodoForm(request.POST, instance=edit)
        if form.is_valid():
            edit = form.save()
            return redirect("todo_list_detail", id=edit.id)
    else:
        form = TodoForm(instance=edit)
    context = {
        "form": form,
    }
    return render(request, "todos/update.html", context)


def todo_list_delete(request, id):
    this_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        this_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create_items.html", context)


def edit_item(request, id):
    edit_i = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=edit_i)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=edit_i)
    context = {
        "list_object": edit_i,
        "post_form": form,
    }
    return render(request, "todos/edititem.html", context)
